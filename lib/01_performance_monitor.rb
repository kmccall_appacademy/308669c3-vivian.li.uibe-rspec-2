def measure(number = 1)
  start_time = Time.now
  number.times do
    yield
  end
  (Time.now - start_time) / number

end
