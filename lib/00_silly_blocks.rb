def reverser
  yield.split.map(&:reverse).join(' ')
end

def adder(add_value = 1)
  yield + add_value
end

def repeater(repeat_times = 1)
  repeat_times.times { yield }
end
